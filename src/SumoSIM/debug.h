/*
    Sumo|SIM - A simulator for use in the Sumo|UY robotic fight events.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Sumo|SIM.

    Sumo|SIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Sumo|SIM (2009 - Steven Rodriguez -)                                 //
//=====================================================================//

////////////////////////////////////////////////////////////////////
/// \file debug.h
/// \brief Sumo|SIM debug file.
/// \details This is the debug file of the Sumo|SIM program.
////////////////////////////////////////////////////////////////////

//===========================================================//
//Libraries                                                  //
//===========================================================//

#if !defined(SUMOSIMHEADER)

#include "sumosim.h"

#endif

//===========================================================//
//Functions                                                  //
//===========================================================//

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 Debug_Load(SumoUY_UInt8 type, char *filename)
/// \brief Loads the debug system.
/// \param type The debug system type.
/// \param filename The debug filename.
/// \return SUMOUY_TRUE if loads and SUMOUY_FALSE if not.
////////////////////////////////////////////////////
SumoUY_UInt8 Debug_Load(SumoUY_UInt8 type, char *fileName);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 Debug_Unload()
/// \brief Unloads the debug system.
/// \return SUMOUY_TRUE if unloads correctly and SUMOUY_FALSE if not.
////////////////////////////////////////////////////
SumoUY_UInt8 Debug_Unload();

////////////////////////////////////////////////////
/// \fn void Debug_Write(char *message)
/// \brief Writes a message in the debug system.
/// \param message The message to write.
////////////////////////////////////////////////////
void Debug_Write(char *message);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 Debug_GetType()
/// \brief Gets the debug system type.
/// \return The debug system type.
////////////////////////////////////////////////////
SumoUY_UInt8 Debug_GetType();

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 Debug_SetEvent(void (*OnDebugMessage)(char *message))
/// \brief Sets the function for debug events.
/// \return SUMOUY_TRUE if teh function is accepted and SUMOUY_FALSE if not.
////////////////////////////////////////////////////
SumoUY_UInt8 Debug_SetEvent(void (*OnDebugMessage)(char *message));
