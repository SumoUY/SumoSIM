/*
    Sumo|SIM - A simulator for use in the Sumo|UY robotic fight events.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Sumo|SIM.

    Sumo|SIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Sumo|SIM (2009 - Steven Rodriguez -)                                 //
//=====================================================================//

////////////////////////////////////////////////////////////////////
/// \file physics.h
/// \brief Sumo|SIM physics interface file.
/// \details This is the physics file of the Sumo|SIM program.
////////////////////////////////////////////////////////////////////

//===========================================================//
//Libraries                                                  //
//===========================================================//

#if !defined(SUMOSIMHEADER)

#include "sumosim.h"

#endif

#include <ode/ode.h>

//===========================================================//
//Structures                                                  //
//===========================================================//

/*struct Robot
{
    dBodyID *Bodies;
    dGeomID *Geoms;
    dJointID *Joints;
};*/

//===========================================================//
//Functions                                                  //
//===========================================================//

SumoUY_UInt8 Physics_Initialize();
void Physics_Close();
SumoUY_UInt8 Physics_ProcessStep(struct SumoController *controller);
