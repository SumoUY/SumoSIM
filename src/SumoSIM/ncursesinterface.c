/*
    Sumo|SIM - A simulator for use in the Sumo|UY robotic fight events.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Sumo|SIM.

    Sumo|SIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Sumo|SIM (2009 - Steven Rodriguez -)                                 //
//=====================================================================//
#if defined(SUMOSIM_UI_NCURSES)
//=========================================//
//Libraries                                //
//=========================================//

#include "ncursesinterface.h"

//=========================================//
//Internal variables                       //
//=========================================//

//Console specific
WINDOW *consolewin;
GString *text;

//UI specific
static SumoUY_UInt32 framespersecond;
static SumoUY_UInt8 finish;
static SumoUY_UInt8 hasjoystick;
static SDL_Joystick *joystick;
static SumoUY_Int8 currentspeed;
static SumoUY_UInt8 ai;

//Fps calculation
static SumoUY_UInt32 frametime;
static SumoUY_UInt32 starttime;
static SumoUY_UInt32 endtime;
static SumoUY_Int32 totaltime;
static SumoUY_UInt32 actualfps;

//Debug specific
static GString *debugstring;

//Sumo|LIB specific
static struct SumoController *sumocontroller;
static struct Vector2 dcenter;
static SumoUY_UInt32 dradius;
static struct BotSpeed bot1speed;
static struct BotSpeed bot2speed;

//=========================================//
//Functions                                //
//=========================================//

void NCurses_LoadInterface(struct SumoController *controller, struct Vector2 dohyoCenter, SumoUY_UInt32 dohyoRadius, SumoUY_UInt32 fps, SumoUY_Int8 fullscreen, SumoUY_UInt32 width, SumoUY_UInt32 height, SumoUY_UInt8 bpp, SumoUY_UInt8 useJoystick)
{
    //Initialize variables
    debugstring = g_string_new(NULL);
    currentspeed = 0;
    ai = SUMOUY_FALSE;
    bot1speed = SumoUY_CreateBotSpeed(0, 0);
    bot2speed = SumoUY_CreateBotSpeed(0, 0);

    //Obtain variables
    sumocontroller = controller;
    dcenter = dohyoCenter;
    dradius = dohyoRadius;
    framespersecond = fps;
    hasjoystick = useJoystick;

    if(hasjoystick == SUMOUY_TRUE)
	{
	    //Initialize joystick and timer
		if(SDL_Init(SDL_INIT_JOYSTICK | SDL_INIT_TIMER) == -1)
		{
			Debug_Write("Could not init joystick");
			g_string_free(debugstring, TRUE);
			return;
		}

        //Check if number of joysticks is >= 1
		if(SDL_NumJoysticks() == 0)
		{
			Debug_Write("Joystick not found. Please, connect a joystick");
			g_string_free(debugstring, TRUE);
			return;
		}

        //Open joystick
		joystick = SDL_JoystickOpen(0);

		if(joystick == NULL)
		{
			Debug_Write("Can't open joystick");
			g_string_free(debugstring, TRUE);
			return;
		}

		//Select joystick to update manually
		SDL_JoystickEventState(SDL_IGNORE);
	}
	else
	{
	    //Initialize timer
		if(SDL_Init(SDL_INIT_TIMER) == -1)
		{
			Debug_Write("Could not init timer");
			g_string_free(debugstring, TRUE);
			return;
		}
	}

    //Initialize NCurses
    consolewin = initscr();

    if(consolewin == NULL)
    {
        Debug_Write("Could not init Ncurses");
        g_string_free(debugstring, TRUE);
        return;
    }

    //Check for columns and rows specified
	if(COLS <= 85 && LINES <= 25)
	{
		endwin();
		while(isendwin() == FALSE);
		Debug_Write("You must to have at least 85 columns and 25 rows!");
        g_string_free(debugstring, TRUE);
		return;
	}

    //Special keypad function
	if(keypad(consolewin,TRUE) != OK)
	{
		Debug_Write("Can't select keypad. Prepare for hell...");
	}

    //Special nodelay function
	if(nodelay(consolewin,TRUE) != OK)
    {
        Debug_Write("Can't select nodelay. Prepare for hell...");
    }

    //Hide the cursor
	if(curs_set(0) == ERR)
	{
		Debug_Write("Can't hide cursor");
	}

	//Start color system
	if(start_color() == ERR)
	{
		Debug_Write("Can't start console coloring system");
	}

	//Initialize text
	text = g_string_new(NULL);

	//Change debug function
    Debug_SetEvent(NCurses_Debug);

    //Initialize the frame limiter
    frametime = 1000 / framespersecond;

    //NCurses main loop
    finish = SUMOUY_FALSE;

    while(finish == SUMOUY_FALSE)
    {
        //----------------------------
		//Frame limiter function
		starttime = SDL_GetTicks();
		//----------------------------

        //Check for errors
        if(SumoUY_GetError() != SUMOUY_NO_ERROR)
        {
                Debug_Write(SumoUY_GetErrorString());
                break;
        }

        //Check input
        NCurses_CheckInput();

        //Process physics and process the controller data
        //Physics_ProcessStep(sumocontroller);

        if(SumoUY_ControllerProcessData(controller) == SUMOUY_FALSE)
        {
            Debug_Write(SumoUY_GetErrorString());
        }

        //Show Frame
        NCurses_ShowFrame();

        //----------------------------
        //Frame limiter function
        endtime = SDL_GetTicks();

        totaltime = frametime - (endtime - starttime);

		if(totaltime > 0)
		{
			SDL_Delay(frametime - (endtime - starttime));
		}

		endtime = SDL_GetTicks();
		actualfps = 1000 / (endtime - starttime);
        //----------------------------
    }

    //Unload NCurses, text and joystick
    g_string_free(text, TRUE);
    endwin();

    while(isendwin() == FALSE);

    if(hasjoystick == SUMOUY_TRUE)
    {
        if(joystick != NULL)
		{
			SDL_JoystickClose(joystick);
		}
    }
}

void NCurses_CheckInput()
{
    if(hasjoystick == SUMOUY_TRUE) //Check joystick input
    {
        //Poll joystick status
		SDL_JoystickUpdate();

		//Check QUIT action
		if(SDL_JoystickGetButton(joystick,8) == 1)
		{
			finish = SUMOUY_TRUE;
            return;
		}
    }
    else //Check keyboard input
    {
        switch(getch())
        {
            case 'q': //Check QUIT action
                finish = SUMOUY_TRUE;
                return;
        }
    }
}

void NCurses_ShowFrame()
{
    //Clears the screen
	erase();

	//Initialize the yellow color
	if(has_colors() == TRUE)
	{
		init_pair(1,COLOR_YELLOW,COLOR_BLACK);
		attron(COLOR_PAIR(1) | A_BOLD);
	}

	//Draw FPS
	g_string_printf(text, "FPS: %i", actualfps);
	mvprintw(0, 0, text->str);

	//Draw bot 1 position
	g_string_printf(text, "Bot 1 Position: X:%i Y:%i Z:%i", SumoUY_ControllerGetBot1Position(sumocontroller).X, SumoUY_ControllerGetBot1Position(sumocontroller).Y, SumoUY_ControllerGetBot1Position(sumocontroller).Z);
	NCurses_CheckSumoLIBError();
    mvprintw(1, 0, text->str);

    //Draw bot 2 position
	g_string_printf(text, "Bot 2 Position: X:%i Y:%i Z:%i", SumoUY_ControllerGetBot2Position(sumocontroller).X, SumoUY_ControllerGetBot2Position(sumocontroller).Y, SumoUY_ControllerGetBot2Position(sumocontroller).Z);
	NCurses_CheckSumoLIBError();
    mvprintw(2, 0, text->str);

	//Draw bot 1 rotation
	g_string_printf(text, "Bot 1 Rotation: X:%hu Y:%hu Z:%hu", SumoUY_ControllerGetBot1Rotation(sumocontroller).X, SumoUY_ControllerGetBot1Rotation(sumocontroller).Y, SumoUY_ControllerGetBot1Rotation(sumocontroller).Z);
	NCurses_CheckSumoLIBError();
    mvprintw(3, 0, text->str);

    //Draw bot 2 rotation
	g_string_printf(text, "Bot 2 Rotation: X:%hu Y:%hu Z:%hu", SumoUY_ControllerGetBot2Rotation(sumocontroller).X, SumoUY_ControllerGetBot2Rotation(sumocontroller).Y, SumoUY_ControllerGetBot2Rotation(sumocontroller).Z);
	NCurses_CheckSumoLIBError();
    mvprintw(4, 0, text->str);

	//Draw bot 1 current speed
	g_string_printf(text, "Bot 1 speed: Left wheel:%hhd Right wheel:%hhd", bot1speed.LeftWheelSpeed, bot1speed.RightWheelSpeed);
	NCurses_CheckSumoLIBError();
	mvprintw(5, 0, text->str);

	//Draw bot 2 current speed
	g_string_printf(text, "Bot 2 speed: Left wheel:%hhd Right wheel:%hhd", bot2speed.LeftWheelSpeed, bot2speed.RightWheelSpeed);
	NCurses_CheckSumoLIBError();
	mvprintw(6, 0, text->str);

	//Unload the red color and load the green color
    if(has_colors() == TRUE)
    {
        attroff(COLOR_PAIR(2) | A_BOLD);
        init_pair(6,COLOR_GREEN,COLOR_BLACK);
        attron(COLOR_PAIR(6) | A_BOLD);
    }

	//Draw logo

	//Unload the red color and load the white color
	if(has_colors() == TRUE)
	{
		attroff(COLOR_PAIR(6) | A_BOLD);
		init_pair(3,COLOR_WHITE,COLOR_BLACK);
		attron(COLOR_PAIR(3) | A_BOLD);
	}
	mvprintw(12,(COLS - 14) / 2,"Sumo Simulator");

	//Unload the white color and load the red color
	if(has_colors() == TRUE)
	{
		attroff(COLOR_PAIR(3) | A_BOLD);
		init_pair(2,COLOR_RED,COLOR_BLACK);
		attron(COLOR_PAIR(2) | A_BOLD);
	}

	mvprintw(13,(COLS - 47) / 2," ____                        _ ____ ___ __  __ ");
	mvprintw(14,(COLS - 47) / 2,"/ ___| _   _ _ __ ___   ___ | / ___|_ _|  \\/  |");
	mvprintw(15,(COLS - 47) / 2,"\\___ \\| | | | '_ ` _ \\ / _ \\| \\___ \\| || |\\/| |");
	mvprintw(16,(COLS - 47) / 2," ___) | |_| | | | | | | (_) | |___) | || |  | |");
	mvprintw(17,(COLS - 47) / 2,"|____/ \\__,_|_| |_| |_|\\___/| |____/___|_|  |_|");
	mvprintw(18,(COLS - 47) / 2,"                            |_|                ");

	//Unload the red color and load the red color
	if(has_colors() == TRUE)
	{
		attroff(COLOR_PAIR(2) | A_BOLD);
		init_pair(4,COLOR_BLUE,COLOR_BLACK);
		attron(COLOR_PAIR(4) | A_BOLD);
	}
	mvprintw(20,(COLS - 39) / 2,"A Simulator for the Sumo|UY fight event");

	//Initialize the color
    if(has_colors() == TRUE)
    {
        attroff(COLOR_PAIR(2) | A_BOLD);
        init_pair(5,COLOR_WHITE,COLOR_BLACK);
        attron(COLOR_PAIR(5) | A_BOLD);
    }

    mvprintw(LINES - 2,(COLS - debugstring->len) / 2,debugstring->str);

    //Unload colors
    if(has_colors() == TRUE)
    {
        attroff(COLOR_PAIR(5) | A_BOLD);
    }

	//Swap buffers
	refresh();
}

void NCurses_Debug(char *message)
{
    g_string_printf(debugstring, "%s", message);
}

void NCurses_CheckSumoLIBError()
{
    if(SumoUY_GetError() != SUMOUY_NO_ERROR)
    {
        Debug_Write(SumoUY_GetErrorString());
    }
}
#endif
