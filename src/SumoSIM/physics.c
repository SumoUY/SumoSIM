/*
    Sumo|SIM - A simulator for use in the Sumo|UY robotic fight events.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Sumo|SIM.

    Sumo|SIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Sumo|SIM (2009 - Steven Rodriguez -)                                 //
//=====================================================================//

//=========================================//
//Libraries                                //
//=========================================//

#include "physics.h"

//=========================================//
//Internal variables                       //
//=========================================//

//ODE specific
static dWorldID world;
//static dSpaceID space;

//Sumo|LIB specific

//=========================================//
//Functions                                //
//=========================================//

SumoUY_UInt8 Physics_Initialize()
{
    //Initialize ODE
    dInitODE();

    //Create world
    world = dWorldCreate();

    //Set the gravity
    dWorldSetGravity(world, 0.0, 0.0, -9.81);

    return SUMOUY_TRUE;
}

void Physics_Close()
{
    //Destroy world
    dWorldDestroy(world);

    //Close ODE
    dCloseODE();
}

SumoUY_UInt8 Physics_ProcessStep(struct SumoController *controller)
{
    return SUMOUY_TRUE;
}
