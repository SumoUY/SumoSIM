/*
    Sumo|SIM - A simulator for use in the Sumo|UY robotic fight events.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Sumo|SIM.

    Sumo|SIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Sumo|SIM (2009 - Steven Rodriguez -)                                 //
//=====================================================================//

//=========================================//
//Libraries                                //
//=========================================//

#include "debug.h"

//=========================================//
//Internal variables                       //
//=========================================//

static FILE *debugfile;
static SumoUY_UInt8 debugtype;
static SumoUY_UInt8 debugloaded = SUMOUY_FALSE;
static void (*ondebugmessage)(char *message);

//=========================================//
//Functions                                //
//=========================================//

SumoUY_UInt8 Debug_Load(SumoUY_UInt8 type, char *fileName)
{
	if(debugloaded == SUMOUY_FALSE)
	{
		debugtype = type;

		if((debugtype == WriteInFile) && (fileName != NULL))
		{
			debugfile = fopen(fileName,"w");

			if(debugfile == NULL)
			{
				return SUMOUY_FALSE;
			}
		}

        ondebugmessage = NULL;
		debugloaded = SUMOUY_TRUE;

		return SUMOUY_TRUE;
	}

	return SUMOUY_FALSE;
}

SumoUY_UInt8 Debug_Unload()
{
	if(debugloaded == SUMOUY_TRUE)
	{
		if(debugtype == WriteInFile)
		{
			fclose(debugfile);
		}

		debugloaded = SUMOUY_FALSE;

		return SUMOUY_TRUE;
	}

	return SUMOUY_FALSE;
}

void Debug_Write(char *message)
{
	if(debugloaded == SUMOUY_TRUE)
	{
		switch(debugtype)
		{
			case WriteInEvent:
                if(ondebugmessage != NULL)
                    ondebugmessage(message);
				break;
			case WriteInFile:
				fprintf(debugfile, "%s\n", message);
				break;
		}
	}
}

SumoUY_UInt8 Debug_GetType()
{
	return debugtype;
}

SumoUY_UInt8 Debug_SetEvent(void (*OnDebugMessage)(char *message))
{
    if(OnDebugMessage == NULL)
        return SUMOUY_FALSE;

    ondebugmessage = OnDebugMessage;

    return SUMOUY_TRUE;
}

