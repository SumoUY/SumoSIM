/*
    Sumo|SIM - A simulator for use in the Sumo|UY robotic fight events.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Sumo|SIM.

    Sumo|SIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Sumo|SIM (2009 - Steven Rodriguez -)                                 //
//=====================================================================//
#if !defined(SUMOSIM_UI_NCURSES)

#error "Please, compile with a least one interface"

#endif
//===========================================================//
//Libraries                                                  //
//===========================================================//

#include "sumosim.h"

#if defined(SUMOSIM_UI_NCURSES)

#include "ncursesinterface.h"

#endif

//===========================================================//
//Internal Variables                                         //
//===========================================================//

//Configuration specific
static char *debugfile;
static struct Vector2 dohyocenter;
static SumoUY_Int32 dohyoradius;

//Sumo|LIB specific
static struct SumoController *controller;
static struct NetworkAddress controlleraddress;
static struct NetworkAddress serveraddress;

//UI specific
#if defined(SUMOSIM_UI_NCURSES)
static SumoUY_UInt8 ncurses;
#endif
static SumoUY_UInt32 fps;
static SumoUY_Int8 fullscreen;
static SumoUY_UInt32 width;
static SumoUY_UInt32 height;
static SumoUY_UInt8 bpp;
static SumoUY_UInt8 usejoystick;

//Command line arguments
static GOptionEntry entries[] =
{
    {"debugfile", 'd', 0, G_OPTION_ARG_STRING, &debugfile,"Debug filename", "FILE"},
    {"dohyo-center-X", 'x', 0, G_OPTION_ARG_INT, &dohyocenter.X,"Dohyo center (X coordinate)", "INT"},
    {"dohyo-center-y", 'y', 0, G_OPTION_ARG_INT, &dohyocenter.Y,"Dohyo center (Y coordinate)", "INT"},
    {"dohyo-radius", 'r', 0, G_OPTION_ARG_INT, &dohyoradius,"Dohyo radius", "INT"},
    {"controller-host", 'h', 0, G_OPTION_ARG_STRING, &controlleraddress.Host,"Controller host", "STRING"},
    {"controller-port", 'p', 0, G_OPTION_ARG_INT, &controlleraddress.Port,"Controller port", "INT"},
    {"server-host", 'k', 0, G_OPTION_ARG_STRING, &serveraddress.Host,"Server host", "STRING"},
    {"server-port", 'l', 0, G_OPTION_ARG_INT, &serveraddress.Port,"Server port", "INT"},
    {"frames-per-second", 'f', 0, G_OPTION_ARG_INT, &fps,"Frames per second", "INT"},
    {"fullscreen", 'g', 0, G_OPTION_ARG_NONE, &fullscreen,"Fullscreen", NULL},
    {"screen-width", 'w', 0, G_OPTION_ARG_INT, &width,"Screen width", "INT"},
    {"screen-height", 'e', 0, G_OPTION_ARG_INT, &height,"Screen height", "INT"},
    {"bits-per-pixel", 'b', 0, G_OPTION_ARG_INT, &bpp,"Bits per pixel", "INT"},
    {"use-joystick", 'j', 0, G_OPTION_ARG_NONE, &usejoystick,"Use joystick", NULL},
#if defined(SUMOSIM_UI_NCURSES)
    {"ncurses", 'n', 0, G_OPTION_ARG_NONE, &ncurses,"Ncurses interface", NULL},
#endif
    {NULL}
};
static GOptionContext *context;

//Interface
static struct SumoSIMInterface interface;

//===========================================================//
//Functions                                                  //
//===========================================================//

int main(int argc, char *argv[])
{
    //Parse parameters
    context = g_option_context_new("- Sumo|SIM simulator for the Sumo|UY competition.");

    g_option_context_add_main_entries(context, entries, "GETTEXT_PACKAGE");
    g_option_context_set_help_enabled(context, TRUE);

    if(g_option_context_parse(context, &argc, &argv, NULL) == FALSE)
    {
        printf("%s", g_option_context_get_help(context, TRUE, NULL));
        g_option_context_free(context);
        return 0;
    }

    //Fill the restant parameters
    if(dohyoradius == 0)
        dohyoradius = 750;
    if(controlleraddress.Host == NULL)
        controlleraddress.Host = "127.0.0.1";
    if(controlleraddress.Port == 0)
        controlleraddress.Port = 7001;
    if(serveraddress.Host == NULL)
        serveraddress.Host = "127.0.0.1";
    if(serveraddress.Port == 0)
        serveraddress.Port = 8001;
    if(fps == 0)
        fps = 30;
    if(width == 0)
        width = 640;
    if(height == 0)
        height = 480;
    if(bpp == 0)
        bpp = 24;

    //Initialize debug system
    if(debugfile != NULL)
    {
        if(Debug_Load(WriteInFile, debugfile) == SUMOUY_FALSE)
        {
            printf("Could not load debug system");
            return 0;
        }
    }
    else
    {
        if(Debug_Load(WriteInEvent, NULL) == SUMOUY_FALSE)
        {
            printf("Could not load debug system");
            return 0;
        }

        Debug_SetEvent(WriteDebugMessage);
    }

    //Initialize Sumo|LIB
    if(SumoUY_Init() == SUMOUY_FALSE)
    {
        Debug_Write(SumoUY_GetErrorString());
        Debug_Unload();
        g_option_context_free(context);
        SumoUY_Close();
        return 0;
    }

    //Create bot and check if creates
    controller = SumoUY_CreateSumoController(serveraddress, controlleraddress);

    if(controller == NULL)
    {
        Debug_Write(SumoUY_GetErrorString());
        Debug_Unload();
        g_option_context_free(context);
        SumoUY_Close();
        return 0;
    }

    //Load the UI
#if defined(SUMOSIM_UI_NCURSES)
    if(ncurses == SUMOUY_TRUE)
    {
        interface.LoadInterface = NCurses_LoadInterface;
    }
    else
    {
        Debug_Write("Please, select an interface");
        Debug_Unload();
        g_option_context_free(context);
        SumoUY_Close();
        return 0;
    }
#endif
    //Show the interface
    interface.LoadInterface(controller, dohyocenter, dohyoradius, fps, fullscreen, width, height, bpp, usejoystick);

    //Close the Sumo|LIB , debug system and the context
    Debug_Unload();
    g_option_context_free(context);
    SumoUY_Close();

    return 0;
}

void WriteDebugMessage(char *message)
{
    printf("%s\n", message);
}
